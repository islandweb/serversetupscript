#GoGoServer

This is the GoGoServer shell script. It aims to automate the set up of a web server on  Ubuntu 16.04.x with a given domain name. The user has a choice of selecting http or https, and the option to install Wordpress in the root directory.

###The webserver is a LEMP stack:

* **Linux Ubuntu 16.04.x88**
* **NGINX**
* **MariaDB**
* **PHP7.0-fpm**

**LetsEncrypt** is used to create an SSL certificate if https is selected by the user. **Certbot** is used to install the certs, and a daily cron is set up to renew certs.

####Version 1.0


#Setup:

Simply clone this repo into a fresh Ubuntu 16.04.x machine and run the gogoserver.sh script. Follow the instructions presented to you by the script.

1 - SSH into server

`ssh user@SERVER_IP`

2 - Navigate to a suitable directory

`cd ~`
 
3 - Clone the repo

`git clone https://island-web@bitbucket.org/islandweb/serversetupscript.git`

4 - CD into the directory

`cd serversetupscript`

5 - Run the script

`./gogoserver.sh`

6 - Follow instructions in the script.

## If you'd like to see the noisy output of the commands, open new local terminal windown, then:

1 - SSH into server

`ssh user@SERVER_IP`

2 - Navigate to the directory used above

`cd ~/serversetupscript`

3 - Use tail to see what's happening. *Note - you can only do this once the main script has been started, as above.

`tail -f logs/stdout.txt`


#WP install in a sub-directory Setup:

Simply clone this repo and run the new-wp.sh script. Follow the instructions presented to you by the script. Backup your current config/files/database first - this hasn't been thoroughly tested and could fudge your shiz...

1 - SSH into server

`ssh user@SERVER_IP`

2 - Navigate to a suitable directory

`cd ~`
 
3 - Clone the repo

`git clone https://island-web@bitbucket.org/islandweb/serversetupscript.git`

4 - CD into the directory

`cd serversetupscript`

5 - Run the script

`./new-wp.sh`

6 - Follow instructions in the script.


###Stuck? Something to add/suggest?
This is a work in progress, and I'd love to hear suggestions. Any questions, contact jamie@island-web.ca