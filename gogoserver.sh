#!/bin/sh
### This script will make turn an Ubuntu 16.x machiene into a working webserver... With any luck. ###

## Colours ##
NC='\033[0m' ## NC = No Colour
RED='\033[0;31m'
GREEN="\033[0;32m"


## Variables ##

DIR="$( cd "$( dirname "$0" )" && pwd )" #eg /root/serversetupscript
username=""
webname=""
altwebname=""
numberOfDomains="1"
startinstall="0"
https="http"
ipaddress=""
dbpass="SECRET!"
wordpress="0"
wpdbname=""
wpdbuser=""
wpdbpass=""
wpdbuserunquote=""
wpdunquote=""
sftpuser=""
sftpusername=""
sftppassword=""

## Cleanup! ##
rm resources/secureMDB.sh  2> /dev/null
rm resources/default  2> /dev/null
rm -r logs   2> /dev/null

## Make Log File ##
mkdir logs
touch logs/stdout.txt

#
#Intro
#
## echo "Current directory is ${DIR}"
read -p "Yo, what's your name? " username
echo "Wasson ${username}?"
sleep 1
echo "${GREEN}Let's get this server up and running!${NC}"
sleep 1
echo "I just need to know a few things..."
sleep 1

#
#Collect variables and make files accordingly...
#


#
#IP finding...
#
echo "I'm going to try to find your IP address..."
sleep 1
## Variable Automatic Collection ##
ipaddress="$(dig +short myip.opendns.com @resolver1.opendns.com)"
echo "I think it's ${GREEN}${ipaddress}${NC}"

## IP confirmation ##
read -r -p "Is that correct? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        echo "${GREEN}Perfect!${NC}"
        ;;
    *)
## Variable Maual Collection ##
        read -p "Ok, you're going to have to enter it for me... " ipaddress
        echo "Thanks!"
        ;;
esac
sleep 1


#
#https?
#
## Variable Collection ##
read -r -p "Do you want to use https? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        https="https"
        cp resources/default.https resources/default
        perl -pi -e "s/SERVER_IP/${ipaddress}/g" resources/default
        echo "Nice! This is looking tasty."
        ;;
    *)
        cp resources/default.http resources/default
        https="http"
        echo "No worries... http it is!"
        ;;
esac
sleep 1

#
#SFTP?
#
## Variable Collection ##
read -r -p "Do you want set up an SFTP user? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        sftpuser="yes"
        echo "Sweet! Lets get that user set up"
        if [ $(id -u) -eq 0 ]; then
            read -p "Enter username : " sftpusername
            read -p "Enter password : " sftppassword
            egrep "^$username" /etc/passwd >/dev/null
                if [ $? -eq 0 ]; then
                    echo "$username exists!"
                    exit 1
                else
                    pass=$(perl -e 'print crypt($ARGV[0], "password")' $sftppassword)
                    useradd -m -p $pass $sftpusername
                    [ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
        fi
        else
            echo "Only root may add a user to the system"
            exit 2
        fi
        ;;
    *)
        sftpuser="no"
        echo "No worries..."
        ;;
esac
sleep 1


#
#Domain Name(s)
#
## Variable Collection ##
read -p "What's your domain name? " webname

echo "Ooh, she's a beauty..."
perl -pi -e "s/WEBNAME1/${webname}/g" resources/default
perl -pi -e "s/DOMNAME/${webname}/g" resources/default
numberOfDomains="1"
sleep 1


## DNS rule question ##
if [ "$https" = "https" ]; then
    read -r -p "Have you made a DNS rule to point your domain name to this server's IP address? [y/N] " response
    case $response in
        [yY][eE][sS]|[yY])
            echo "${GREEN}You're on the ball!${NC}"
            ;;
        *)
            echo "${RED}Well you need to do that first so LetsEncrypt will work! Do it now and run this script again${NC}"
            exit
            ;;
    	esac
fi

#
#Maria DB Password
#

read -p "Enter a password for your MariaDB root user. Make it strong! :" dbpass

## Write the mariadb secure script with users pass... ##
echo "#!/bin/sh" > resources/secureMDB.sh
echo "" >> resources/secureMDB.sh
echo "mysql -e \"UPDATE mysql.user SET Password=PASSWORD('{${dbpass}}') WHERE User='root';\"" >> resources/secureMDB.sh
echo "mysql -e \"DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');\"" >> resources/secureMDB.sh
echo "mysql -e \"DELETE FROM mysql.user WHERE User='';\"" >> resources/secureMDB.sh
echo "mysql -e \"DROP DATABASE IF EXISTS test;\"" >> resources/secureMDB.sh
echo "mysql -e \"FLUSH PRIVILEGES;\"" >> resources/secureMDB.sh
echo "${GREEN}Beauty!${NC}"
sleep 1

#
# Wordpress vars
#

read -r -p "Would you like to install WordPress in the root directory? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        echo "${GREEN}Kl, in that case, there's a few things I need to know...${NC}"
        wordpress="1"
        sleep 1
        read -p "Enter a database name for your wordpress databse (I recommend wp_name) :" wpdbname
        read -p "Enter a username for your wordpress databse (Standard practice is same as the database name (${wpdbname})) :" wpdbuser
        read -p "Enter a password for wordpress database user. Make it strong! :" wpdbpass
        echo "${GREEN}Thanks!${NC}"
        wpdunquote=${wpdbname}
        wpdbuserunquote=${wpdbuser}
        wpdbname="'${wpdbname}'"
        wpdbpass="'${wpdbpass}'"
        wpdbuser="'${wpdbuser}'"
        ;;
    *)
        wordpress="0"
        echo "${RED}No worries${NC}"
        ;;
esac
sleep 1


#
#
#Confirm Variables...
#
#
echo "OK So this is what I have:"
sleep 1
echo "Your name is ${GREEN}${username}${NC}"
sleep 1
echo "You want to set up a webserver for the domain(s):"
echo "${GREEN}${webname} ${altwebname}${NC}"
sleep 1
echo "This server's IP address is ${GREEN}${ipaddress}${NC}"
sleep 1
echo "And you want to use ${GREEN}${https}${NC}"
sleep 1
if [ "$wordpress" = "1" ]; then
    echo "And you want me to ${GREEN}install WordPress${NC} for you"
fi
sleep 1
if [ "$sftpuser" = "yes" ]; then
    echo "As well as ${GREEN}add an SFTP account${NC} with username ${GREEN}${sftpusername}${NC}"
fi

## Confirmation ##
read -r -p "Is this correct? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        echo "${GREEN}Sweet! Let's get cracking!${NC}"
        startinstall="1"
        ;;
    *)
        startinstall="0"
        echo "${RED}Try running this script again and enter the correct info ;)${NC}"
        exit
        ;;
esac


#
#
#Start doing shit!
#
#

## Update and upgrade confirmation ##
read -r -p "Do you want to run a highly recommended quick and quiet update and upgrade? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        echo "${GREEN}Nice!${NC}"
        echo "${GREEN}Updating${NC}"
        apt-get -qq update -y --force-yes
        echo "${GREEN}Upgrading${NC}"
        apt-get -qq upgrade -y > logs/stdout.txt 2>&1
        ;;
    *)
        echo "${RED}No worries - I trust you're up to date :)${NC}"
        ;;
esac

## Set up firewall ##
. scripts/firewall.sh

## Setup Swap ##
. scripts/swap.sh

## Install Services ##
. scripts/installs.sh

## Configure Services ##
. scripts/config_services.sh

## Install and configure https Services ##
if [ "$https" = "https" ]; then
    . scripts/https_installs.sh
else
    echo "${GREEN}Skipping https setup${NC}"
    echo "${GREEN}Coppying over actual nginx defult config and restarting services${NC}"
    mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak2
    cp resources/default /etc/nginx/sites-available/default
    service nginx restart
    service mysql restart
    service php7.0-fpm restart
    echo "${GREEN}Checking that NGINX is running properly...${NC}"
    nginx -t
fi

## Install Wordpress ##
if [ "$wordpress" = "1" ]; then
    . scripts/wordpress.sh
fi

## set up sftp user stuff ##
if [ "$sftpuser" = "yes" ]; then
    sudo groupadd sftp  2> /dev/null
    sudo adduser ${sftpusername} sftp  2> /dev/null
    sudo adduser ${sftpusername} www-data  2> /dev/null
    sudo usermod -d /var/www/html ${sftpusername}  2> /dev/null
    echo "\nMatch Group sftp\n\tPasswordAuthentication yes" >> /etc/ssh/sshd_config
    service ssh restart
    chmod -R g+rwX /var/www/html
    chmod -R g+s /var/www/html/
fi

## End Message and cleanup ##
echo "${GREEN}Final Update and Upgrade${NC}"
apt-get -qq update -y --force-yes
apt-get -qq upgrade -y > logs/stdout.txt 2>&1

echo "${GREEN}Restarting NGINX MYSQL and PHP${NC}"
service nginx restart
service mysql restart
service php7.0-fpm restart
if [ "$https" = "https" ]; then
echo ""
echo "${RED}********IMPORTANT****************************************************************************${NC}"
echo ""
echo "${GREEN}First make sure you set 'SSL' to 'Full' and turn ON 'Automatic HTTPS Rewrites' in CloudFlare!${NC}"
echo ""
echo "${RED}*********************************************************************************************${NC}"
echo ""
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
echo "${GREEN}Then you should be all set up and ready to go!${NC}"
echo ""
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
fi
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
echo "${GREEN}All good! Go check it out :D ${NC}"
echo ""
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
## Cleanup! ##
mkdir /var/log/GoGoServer
mv ${DIR}/logs /var/log/GoGoServer/logs
rm -r ../serversetupscript
exit
