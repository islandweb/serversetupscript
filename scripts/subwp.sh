#!/bin/sh

## Wordpress! ##

echo "${GREEN}Now we're gonna get wordpress installed at ${webroot}/${subdirectory}${NC}"

wget -qP ${webroot}/${subdirectory} https://wordpress.org/latest.zip
unzip -qq -d ${webroot}/${subdirectory} ${webroot}/${subdirectory}/latest.zip
mv ${webroot}/${subdirectory}/wordpress/* ${webroot}/${subdirectory}/
rm -rf ${webroot}/${subdirectory}/wordpress
rm ${webroot}/${subdirectory}/latest.zip
chown -R www-data:www-data ${webroot}/${subdirectory}


### Set up database for WP ###

mysql -e "CREATE USER ${wpdbuser}@'localhost' IDENTIFIED BY ${wpdbpass};"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO ${wpdbuser}@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "CREATE DATABASE ${wpdunquote};"


### Set wp-config and cleanup ###

    cp ${webroot}/${subdirectory}/wp-config-sample.php ${webroot}/${subdirectory}/wp-config.php
    perl -pi -e "s/\'database_name_here\'/${wpdbname}/g" ${webroot}/${subdirectory}/wp-config.php
    sleep 1
    perl -pi -e "s/\'username_here\'/${wpdbuser}/g" ${webroot}/${subdirectory}/wp-config.php
    sleep 1
    perl -pi -e "s/\'password_here\'/${wpdbpass}/g" ${webroot}/${subdirectory}/wp-config.php
    sleep 1