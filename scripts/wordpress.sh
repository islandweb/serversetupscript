#!/bin/sh


## Wordpress! ##

echo "${GREEN}Now we're gonna get wordpress installed!${NC}"

wget -qP /var/www/html https://wordpress.org/latest.zip
unzip -qq -d /var/www/html/ /var/www/html/latest.zip
mv /var/www/html/wordpress/* /var/www/html/
rm -rf /var/www/html/wordpress
rm /var/www/html/latest.zip
rm /var/www/html/index.nginx-debian.html
chown -R www-data:www-data /var/www


### Set up database for WP ###

mysql -e "CREATE USER ${wpdbuser}@'localhost' IDENTIFIED BY ${wpdbpass};"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO ${wpdbuser}@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "CREATE DATABASE ${wpdunquote};"


### Set wp-config and cleanup ###

    cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
    perl -pi -e "s/\'database_name_here\'/${wpdbname}/g" /var/www/html/wp-config.php
    sleep 1
    perl -pi -e "s/\'username_here\'/${wpdbuser}/g" /var/www/html/wp-config.php
    sleep 1
    perl -pi -e "s/\'password_here\'/${wpdbpass}/g" /var/www/html/wp-config.php
    sleep 1
    chown -R www-data:www-data /var/www