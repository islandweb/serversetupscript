#!/bin/sh

#
## Add SWAP ## 
#
echo "${GREEN}Now lets add some SWAP${NC}"

fallocate -l 2G /swapfile > logs/stdout.txt 2>&1 

chmod 600 /swapfile > logs/stdout.txt 2>&1 

mkswap /swapfile > logs/stdout.txt 2>&1 

swapon /swapfile > logs/stdout.txt 2>&1 

echo "" >> /etc/fstab #echo nothing first so the next echo appears on a new line in the file.
echo "/swapfile   none    swap    sw    0   0" >> /etc/fstab