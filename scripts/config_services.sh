#!/bin/sh

## NGINX ##

echo "${GREEN}Configuring NGINX!${NC}"
ufw allow 'Nginx Full' > logs/stdout.txt 2>&1
sed -i '/sendfile on/ i \       # set client body size to 2M #\' /etc/nginx/nginx.conf
sed -i '/sendfile on/ i \       client_max_body_size 120M;\' /etc/nginx/nginx.conf
## backup the current default config file ##
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bakup
## copy over our initial default config file ##
cp resources/nginx-default.txt /etc/nginx/sites-available/default
echo "${GREEN}Restarting NGINX${NC}"
service nginx restart
nginx -t > logs/stdout.txt 2>&1


## PHP ##

echo "${GREEN}Configuring PHP${NC}"
phpenmod mcrypt
phpenmod mbstring
perl -pi -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/upload_max_filesize = 2M/upload_max_filesize = 256M/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/post_max_size = 8M/post_max_size = 256M/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/;pm.max_requests = 500/pm.max_requests = 500/g' /etc/php/7.0/fpm/php.ini

## MariaDB ##

echo "${GREEN}Securing MariaDB${NC}"
chmod +x resources/secureMDB.sh
. resources/secureMDB.sh
