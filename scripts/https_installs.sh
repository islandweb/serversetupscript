#!/bin/sh

## https installs ##

echo "${GREEN}Installing Certbot${NC}"
mkdir /opt/certbot
cd /opt/certbot
wget -q https://dl.eff.org/certbot-auto
chmod a+x certbot-auto


echo "${GREEN}Configuring Certbot.. this may take some time${NC}"
		if [ "$numberOfDomains" = "1" ]; then
		  ./certbot-auto certonly -n --webroot --webroot-path /var/www/html --renew-by-default --email jamie@island-web.ca --text --agree-tos -d ${webname}  > ${DIR}/logs/stdout.txt 2>&1 
        else
			if [ "$numberOfDomains" = "2" ]; then
			     ./certbot-auto certonly --webroot --webroot-path /var/www/html --renew-by-default --email jamie@island-web.ca --text --agree-tos -d ${webname} -d ${altwebname}  > ${DIR}/logs/stdout.txt 2>&1 
			fi
		fi

echo "${GREEN}Configuring Certbot cron to auto renew certs${NC}"
cp ${DIR}/resources/certbot-cron.sh /etc/cron.daily/certbot-cron.sh

echo "${GREEN}Generating a strong Diffie-Hellman group... This could take a few minutes or so...${NC}"
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
	
echo "ssl_certificate /etc/letsencrypt/live/${webname}/fullchain.pem;" > /etc/nginx/snippets/ssl-${webname}.conf
echo "ssl_certificate_key /etc/letsencrypt/live/${webname}/privkey.pem;" >> /etc/nginx/snippets/ssl-${webname}.conf
cp ${DIR}/resources/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
mkdir /usr/share/nginx/logs
touch /usr/share/nginx/logs/${webname}-error_log
cd ${DIR}

echo "${GREEN}Coppying over actual nginx defult config and restarting services${NC}"
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak2
cp resources/default /etc/nginx/sites-available/default
service nginx restart
service mysql restart
service php7.0-fpm restart
echo "${GREEN}Checking that NGINX is running properly...${NC}"
nginx -t