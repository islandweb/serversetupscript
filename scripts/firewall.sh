#!/bin/sh

## Allow ssh in ufw ## 
echo "${GREEN}Updating firewall rules${NC}"
ufw allow OpenSSH > logs/stdout.txt 2>&1 

## Turn on ufw ## 
echo "${GREEN}Enabling Firewall${NC}"
ufw --force enable > logs/stdout.txt 2>&1 

#
## Install fail2ban ## 
#
echo "${GREEN}Installing fail2ban${NC}"
apt-get -qq install -y fail2ban > logs/stdout.txt 2>&1 

## Configure Fail2ban ## 
echo "${GREEN}Configuring fail2ban${NC}"
cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
perl -pi -e 's/maxretry = 5/maxretry = 3/g' /etc/fail2ban/jail.local
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -j DROP
echo "${GREEN}Restarting fail2ban${NC}"


## Restart Fail2ban ## 
service fail2ban stop > logs/stdout.txt 2>&1 

service fail2ban start > logs/stdout.txt 2>&1 

echo "${GREEN}Firewall all set up!!${NC}"
