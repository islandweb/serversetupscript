#!/bin/sh

if [ "$https" = "https" ]; then
webname=$(grep -oP 'https://\K.*(?=\$1)' $serverblock)
read -r -p "Is $webname your domain name? [y/N] " response
    case $response in
        [yY][eE][sS]|[yY]) 
            echo "${GREEN}Sweet!${NC}"
            ;;
        *)
            read -p "Well, I tried! Please enter your domain name: " webname
            echo "${GREEN} Thanks!${NC}"
            ;;
    	esac

else
	if [ "$https" = "http" ]; then
	webname=$(grep -oP 'server_name \K.*(?=\;)' $serverblock)
	read -r -p "Is $webname your domain name? [y/N] " response
	    case $response in
	        [yY][eE][sS]|[yY]) 
	            echo "${GREEN}Sweet!${NC}"
	            ;;
	        *)
	            read -p "Well, I tried! Please enter your domain name: " webname
	            echo "${GREEN} Thanks!${NC}"
	            ;;
	    	esac
	fi
fi
