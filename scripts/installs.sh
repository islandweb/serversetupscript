#!/bin/sh

echo "${GREEN}Installing NGINX${NC}"
apt-get -qq install -y nginx > logs/stdout.txt 2>&1 

echo "${GREEN}Installing PHP${NC}"
apt-get -qq install -y php7.0-fpm php7.0-mysql php7.0-mcrypt php-mbstring php-gettext php7.0-gd php-curl > logs/stdout.txt 2>&1 

echo "${GREEN}Installing MariaDB${NC}"
apt-get -qq install -y mariadb-client mariadb-server > logs/stdout.txt 2>&1 

echo "${GREEN}Installing Zip${NC}"
apt-get -qq install -y zip > logs/stdout.txt 2>&1 