#!/bin/sh
### This script will install wordpress on a new subdomain ###

## Colours ##
NC='\033[0m' ## NC = No Colour
RED='\033[0;31m'
GREEN="\033[0;32m"


## Variables ##

username=""
webname=""
altwebname=""
numberOfDomains="1"
startinstall="0"
https="http"
ipaddress=""
dbpass="SECRET!"
wordpress="0"
wpdbname=""
wpdbuser=""
wpdbpass=""
wpdbuserunquote=""
wpdunquote=""
serverblock="/etc/nginx/sites-available/default"
subdirectory=""
webroot="/var/www/html/"



#
#Intro
#
echo "So, you want to install WP in a new subdirectory hey? "
sleep 1
echo "Make sure you backup everything first! If you're using DigitalOcean you should poweroff, create a snapshot of the server and power on again. Then run through this script."
sleep 1

echo "This will only work if you used the ${GREEN}Gogoserver script${NC} to set up this server "
sleep 1
read -r -p "Did you use the Gogoserver script? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "${GREEN}Perfect!${NC}"
        sleep 1
        ;;
    *)
        echo "${RED}Ok, well continue at your own risk.${NC} (hit 'ctrl + c' to stop now!"
        echo "5"
        sleep 1
        echo "4"
        sleep 1
        echo "3"
        sleep 1
        echo "2"
        sleep 1
        echo "1"
        ;;
esac

### http or https ###
read -r -p "Did you set the server up to run https? (y for https, n for http) [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        https="https"
        echo "${GREEN}Nice!${NC}"
        sleep 1
        ;;
    *)
        https="http"
        echo "No worries... ${GREEN}http${NC} it is!"
        sleep 1
        ;;
esac

### Find domain name ###
if [ -f "$serverblock" ]
then
 . scripts/finddomain.sh
else
	echo "I cant find your default server block at $serverblock"
	read -p "Please enter the full path to the serverblock starting with / : " serverblock
	echo "${GREEN}Thanks!${NC}"
	. scripts/finddomain.sh
fi
sleep 1

### check for root directory ###
if [ -f "${webroot}wp-config.php" ]
then
 webroot="/var/www/html"
else
	echo "I cant find your root directory at ${webroot}"
	read -p "Please enter the full path to your root directory starting with / : " webroot
	echo "${GREEN}Thanks!${NC}"
fi
sleep 1

### Collect wp db vars ###
echo "${GREEN}I need to ask a few questions about the new wp installation...${NC}"
sleep 2
echo "${GREEN}This includes database name, user and password${NC}"
sleep 2
echo "${GREEN}The database name and user SHOULD NOT be the same as any current user or password...${NC}"
sleep 2
echo "${GREEN}If this is your second wordpress install on this server, I recommend preceding the database name and user with 'wp2_'${NC}"
sleep 2
echo "${GREEN}If this is your third wordpress install on this server, I recommend preceding the database name and user with 'wp3_'${NC}"
sleep 2
echo "${GREEN}Etc..${NC}"
sleep 1
echo "${GREEN}Etc..${NC}"
sleep 2
echo "${RED}This is important. If you're unsure, check your current wp-config files to remind yourself of existing database credentials before continuing.${NC}"
sleep 1
        read -p "Enter a database name for your wordpress databse (I recommend wp2_name) :" wpdbname
        read -p "Enter a username for your wordpress databse (I recommend wp2_name) :" wpdbuser
        read -p "Enter a password for wordpress database user. Make it strong! :" wpdbpass
        echo "${GREEN}Thanks!${NC}"
        wpdunquote=${wpdbname}
        wpdbuserunquote=${wpdbuser}
        wpdbname="'${wpdbname}'"
        wpdbpass="'${wpdbpass}'"
        wpdbuser="'${wpdbuser}'"

### Collect and check subdirectory var ###
read -p "Please enter the subdirectory in which the new WP should be installed (just the name, no need for domains or slashes): " subdirectory

echo "So your new installation will be accessable at ${GREEN} ${webname}/${subdirectory} ${NC}"

read -r -p "Is that kl? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "${GREEN}Nice!${NC}"
        sleep 1
        ;;
    *)
        read -p "Put the correct subdirectory in then: " subdirectory
        echo "So your new installation will be accessable at ${GREEN} ${webname}/${subdirectory} ${NC}"
        sleep 1
        ;;
esac

### If subdirectory exists, warning. Else create subdirectory and install wordpress ###
if [ -d "${webroot}/${subdirectory}" ]
then
 echo "The subdirectory at ${GREEN} ${webroot}/${subdirectory} ${NC} already exists!"
 echo "Please check in your root directory for the folder ${GREEN} /${subdirectory} ${NC} and then come back here, run this script again and enter a valid subdirectory"
 exit
else
	mkdir ${webroot}/${subdirectory}
	. scripts/subwp.sh
fi

echo "${GREEN}Ok, you should be good to go!${NC}"
sleep 1
echo "${GREEN}Open your browser, and go to ${webname}/${subdirectory}${NC}"
sleep 1
echo "${GREEN}Once set up, the admin for this WP installation will be at ${webname}/${subdirectory}/wp-admin${NC}"
sleep 1
echo "${GREEN}Have fun!${NC}"
