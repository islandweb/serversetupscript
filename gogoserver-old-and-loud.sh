#!/bin/sh
### This script will make turn an Ubuntu 16.x machiene into a working webserver... With any luck. ###

read -r -p "Do you want to run in quiet mode? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "Great!"
        . ./gogoserver-quiet.sh
        ;;
    *)
        echo "No worries..."
        ;;
esac
sleep 1

## Colours ##
NC='\033[0m' ## NC = No Colour
RED='\033[0;31m'
GREEN="\033[0;32m"


## Variables ##

username=""
webname=""
altwebname=""
numberOfDomains="1"
startinstall="0"
https="http"
ipaddress=""
dbpass="SECRET!"
wordpress="0"
wpdbname=""
wpdbuser=""
wpdbpass=""
wpdbuserunquote=""
wpdunquote=""

## Cleanup! ##
rm resources/secureMDB.sh  2> /dev/null
rm resources/default  2> /dev/null

#
# Get users name
#
read -p "Yo, what's your name? " username

#
#Intro
#
echo "Wasson ${username}?"
sleep 1
echo "${GREEN}Let's get this server up and running!${NC}"
sleep 1
echo "I just need to know a few things..."
sleep 1

#
#Collect variables and make files accordingly...
#

#
#Domain Name(s)
#
## Variable Collection ##
read -p "What's your domain name? " webname
read -r -p "Do you want to add www.${webname} too? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        altwebname="www.${webname}"
        numberOfDomains="2"
        echo "Great!"
        ;;
    *)
        echo "No worries..."
        numberOfDomains="1"
        ;;
esac
sleep 1

#
#https?
#
## Variable Collection ##
read -r -p "Do you want to use https? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        https="https"
        echo "Nice! This is looking tasty."
        ;;
    *)
        echo "No worries... http it is!"
        https="http"
        ;;
esac
sleep 1

# Make a new default file for population based on wheather the user wants https or not. ##

if [ "$https" = "https" ]; then

	cp resources/default.https resources/default
		
		if [ "$numberOfDomains" = "1" ]; then
			perl -pi -e "s/WEBNAME1/${webname}/g" resources/default
			perl -pi -e "s/DOMNAME/${webname}/g" resources/default
		else #if user selected 2 domain names...
			if [ "$numberOfDomains" = "2" ]; then
				perl -pi -e "s/WEBNAME1/${webname} ${altwebname}/g" resources/default
				perl -pi -e "s/DOMNAME/${webname}/g" resources/default
			fi
		fi

else #if user selected http...
	if [ "$https" = "http" ]; then

		cp resources/default.http resources/default

		if [ "$numberOfDomains" = "1" ]; then
			perl -pi -e "s/WEBNAME1/${webname}/g" resources/default
		else #if user selected http...
			if [ "$numberOfDomains" = "2" ]; then
				perl -pi -e "s/WEBNAME1/${webname} ${altwebname}/g" resources/default
			fi
		fi
	
	fi
fi


#
#IP finding...
#
echo "I'm just going to find your IP address..."
sleep 1
## Variable Automatic Collection ##
ipaddress="$(dig +short myip.opendns.com @resolver1.opendns.com)"
echo "I think it's ${GREEN}${ipaddress}${NC}"

## IP confirmation ##
read -r -p "Is that correct? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "${GREEN}Perfect!${NC}"
        ;;
    *)
## Variable Maual Collection ##
        read -p "Ok, you're going to have to enter it for me... " ipaddress
        echo "Thanks!"
        ;;
esac
sleep 1

## Writing IP to resources/default ##
if [ "$https" = "https" ]; then
    perl -pi -e "s/SERVER_IP/${ipaddress}/g" resources/default
    read -r -p "Have you made a DNS rule to point your domain name to this IP address? [y/N] " response
    case $response in
        [yY][eE][sS]|[yY]) 
            echo "${GREEN}You're on the ball!${NC}"
            ;;
        *)
            echo "${RED}Well you need to do that first so LetsEncrypt will work! Do it now and run this script again${NC}"
            exit
            ;;
    	esac
fi

#
#Maria DB Password
#

read -p "Enter a password for your MariaDB root user. Make it strong! :" dbpass

## Write the mariadb secure script with users pass... ##
echo "#!/bin/sh" > resources/secureMDB.sh
echo "" >> resources/secureMDB.sh
echo "mysql -e \"UPDATE mysql.user SET Password=PASSWORD('{${dbpass}}') WHERE User='root';\"" >> resources/secureMDB.sh
echo "mysql -e \"DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');\"" >> resources/secureMDB.sh
echo "mysql -e \"DELETE FROM mysql.user WHERE User='';\"" >> resources/secureMDB.sh
echo "mysql -e \"DROP DATABASE IF EXISTS test;\"" >> resources/secureMDB.sh
echo "mysql -e \"FLUSH PRIVILEGES;\"" >> resources/secureMDB.sh
echo "${GREEN}Beauty!${NC}"
sleep 1

#
# Wordpress vars
#

read -r -p "Would you like to install WordPress in the root directory? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "${GREEN}Kl, in that case, there's a few things I need to know...${NC}"
        wordpress="1"
        sleep 1
        read -p "Enter a database name for your wordpress databse (I recommend wp_name) :" wpdbname
        read -p "Enter a username for your wordpress databse (Standard practice is same as the database name (${wpdbname})) :" wpdbuser
        read -p "Enter a password for wordpress database user. Make it strong! :" wpdbpass
        echo "${GREEN}Thanks!${NC}"
        wpdunquote=${wpdbname}
        wpdbuserunquote=${wpdbuser}
        wpdbname="'${wpdbname}'"
        wpdbpass="'${wpdbpass}'"
        wpdbuser="'${wpdbuser}'"
        ;;
    *)
        wordpress="0"
        echo "${RED}No worries${NC}"
        ;;
esac
sleep 1


#
#
#Confirm Variables...
#
#
echo "OK So this is what I have:"
sleep 1
echo "Your name is ${GREEN}${username}${NC}"
sleep 1
echo "You want to set up a webserver for the domain(s):"
echo "${GREEN}${webname} ${altwebname}${NC}"
sleep 1
echo "This server's IP address is ${GREEN}${ipaddress}${NC}"
sleep 1
echo "And you want to use ${GREEN}${https}${NC}"
sleep 1
if [ "$wordpress" = "1" ]; then
    echo "And you want me to ${GREEN}install WordPress${NC} for you"
fi

## Confirmation ##
read -r -p "Is this correct? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo "${GREEN}Sweet! Let's get cracking!${NC}"
        startinstall="1"
        ;;
    *)
        startinstall="0"
        echo "${RED}Try running this script again and enter the correct info ;)${NC}"
        exit
        ;;
esac


#
#
#Start doing shit!
#
#

## Allow ssh in ufw ## 
echo "${GREEN}Updating firewall rules${NC}"
sleep 1
ufw allow OpenSSH
sleep 1

## Turn on ufw ## 
echo "${GREEN}Enabling Firewall${NC}"
sleep 1
ufw --force enable
sleep 1

#
## Install fail2ban ## 
#
echo "${GREEN}Installing fail2ban${NC}"
sleep 1
apt-get install -y fail2ban

## Configure Fail2ban ## 
echo "${GREEN}Configuring fail2ban${NC}"
sleep 1
cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
perl -pi -e 's/maxretry = 5/maxretry = 3/g' /etc/fail2ban/jail.local
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -j DROP
echo "${GREEN}Restarting fail2ban${NC}"
sleep 2

## Restart Fail2ban ## 
service fail2ban stop
sleep 1
service fail2ban start

echo "${GREEN}Firewall all set up!!${NC}"
sleep 2

#
## Add SWAP ## 
#
echo "${GREEN}Now lets add some SWAP${NC}"
sleep 2

fallocate -l 2G /swapfile
sleep 1
chmod 600 /swapfile
sleep 1
mkswap /swapfile
sleep 1
swapon /swapfile
sleep 1
echo "" >> /etc/fstab #echo nothing first so the next echo appears on a new line in the file.
echo "/swapfile   none    swap    sw    0   0" >> /etc/fstab

#
## Update and upgrade using apt ## 
#
echo "${GREEN}Quick Update and Upgrade${NC}"
sleep 2

apt-get update
apt-get upgrade -y

#
##Install LetsEncrypt!! ##
#
if [ "$https" = "https" ]; then

    echo "${GREEN}Installing LetsEncrypt${NC}"
    sleep 2
    apt-get install -y letsencrypt
fi

#
## Install NGINX ##
#
echo "${GREEN}Lets install NGINX!${NC}"
sleep 2
apt-get install -y nginx
sleep 1
ufw allow 'Nginx Full'
sleep 1
ufw status
sleep 1
sed -i '/sendfile on/ i \       # set client body size to 2M #\' /etc/nginx/nginx.conf
sleep 1
sed -i '/sendfile on/ i \       client_max_body_size 120M;\' /etc/nginx/nginx.conf
sleep 1
echo "${GREEN}Restarting NGINX${NC}"
sleep 2
service nginx restart
sleep 3


## backup the current default config file ##
echo "${GREEN}backing up the current default config file${NC}"
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bakup
sleep 1
## copy over our initial default config file ##
echo "${GREEN}copying over our initial nginx default config file${NC}"
cp resources/nginx-default.txt /etc/nginx/sites-available/default
sleep 1
echo "${GREEN}Restarting NGINX${NC}"
service nginx restart
nginx -t
sleep 5

#
## Install PHP7 ##
#
echo "${GREEN}Lets install PHP7!${NC}"
sleep 2
apt-get install -y php7.0-fpm php7.0-mysql php7.0-mcrypt php-mbstring php-gettext php7.0-gd
echo "${GREEN}Enabling php mods${NC}"
sleep 2
phpenmod mcrypt
phpenmod mbstring
perl -pi -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/upload_max_filesize = 2M/upload_max_filesize = 256M/g' /etc/php/7.0/fpm/php.ini
perl -pi -e 's/post_max_size = 8M/post_max_size = 256M/g' /etc/php/7.0/fpm/php.ini

#
## Install MariaDB ##
#
echo "${GREEN}Lets install MarliaDB!${NC}"
sleep 2
apt-get install -y mariadb-client mariadb-server
sleep 1
echo "${GREEN}Making secure MDB script executable...${NC}"
chmod +x resources/secureMDB.sh
sleep 2
echo "${GREEN}Running secureMDB.sh${NC}"
sleep 2 
. resources/secureMDB.sh
sleep 16
echo "${GREEN}Restarting NGINX MYSQL and PHP${NC}"
service nginx restart
service mysql restart
service php7.0-fpm restart

#
##Configure LetsEncrypt!! ##
#
if [ "$https" = "https" ]; then

	echo "${GREEN}Now Configuring LetsEncrypt${NC}"
	sleep 2

		if [ "$numberOfDomains" = "1" ]; then
		  letsencrypt certonly -a webroot --webroot-path=/var/www/html -d ${webname}
		  sleep 2
        else
			if [ "$numberOfDomains" = "2" ]; then
			     sudo letsencrypt certonly -a webroot --webroot-path=/var/www/html -d ${webname} -d ${altwebname}
			     sleep 2
            fi
		fi
    sleep 2
    echo "${GREEN}Generating a strong Diffie-Hellman group${NC}"
    sleep 1
	openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
    sleep 1
    echo "${GREEN}Adding config files${NC}"
    sleep 2
	echo "ssl_certificate /etc/letsencrypt/live/${webname}/fullchain.pem;" > /etc/nginx/snippets/ssl-${webname}.conf
    echo "ssl_certificate_key /etc/letsencrypt/live/${webname}/privkey.pem;" >> /etc/nginx/snippets/ssl-${webname}.conf
    cp resources/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
    mkdir /usr/share/nginx/logs
    touch /usr/share/nginx/logs/${webname}-error_log
else #if user selected http, there's no need for LetsEncrypt...
	if [ "$https" = "http" ]; then
		echo "${GREEN}No need for for LetsEncrypt - you chose http${NC}"
	fi
fi
echo "${GREEN}Coppying over actual nginx defult config and restarting nginx${NC}"
sleep 2
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak2
cp resources/default /etc/nginx/sites-available/default
echo "${GREEN}Restarting NGINX MYSQL and PHP${NC}"
service nginx restart
service mysql restart
service php7.0-fpm restart
sleep 2
echo "${GREEN}Check that NGINX is running properly...${NC}"
sleep 2
nginx -t
sleep 2

#
#
#Wordpress!
#
#
if [ "$wordpress" = "1" ]; then

    echo "${GREEN}Now we're gonna get wordpress installed!${NC}"
    sleep 5

    ### This part needs a good tidy up! (The whole thing probably does!!!!) ###

    cd /var/www/html
    sudo wget https://wordpress.org/latest.zip
    sudo apt-get install -y zip
    sudo unzip latest.zip
    cd wordpress
    sudo mv * /var/www/html
    cd ..
    sudo rm -rf wordpress
    cd /
    sudo chown -R www-data:www-data /var/www


    ### Set up database for WP ###
    echo "${GREEN}Setting up mysql for Wordpress${NC}"
    mysql -e "CREATE USER ${wpdbuser}@'localhost' IDENTIFIED BY ${wpdbpass};"
    mysql -e "GRANT ALL PRIVILEGES ON * . * TO ${wpdbuser}@'localhost';"
    mysql -e "FLUSH PRIVILEGES;"
    mysql -e "CREATE DATABASE ${wpdunquote};"

    ### Set wp-config and cleanup ###
    cd /var/www/html
    rm latest.zip
    rm index.nginx-debian.html
    cp wp-config-sample.php wp-config.php
    perl -pi -e "s/\'database_name_here\'/${wpdbname}/g" wp-config.php
    sleep 1
    perl -pi -e "s/\'username_here\'/${wpdbuser}/g" wp-config.php
    sleep 1
    perl -pi -e "s/\'password_here\'/${wpdbpass}/g" wp-config.php
    sleep 1
    echo "${GREEN}WordPress should be ready!${NC}"
else
    if [ "$wordpress" = "0" ]; then
       echo "${GREEN}No need to install WordPress. You must have other plans...${NC}"
   fi
fi
sleep 2
echo "${GREEN}Restarting NGINX MYSQL and PHP${NC}"
service nginx restart
service mysql restart
service php7.0-fpm restart
echo ""
echo "${RED}********IMPORTANT****************************************************************************${NC}"
echo ""
echo "${GREEN}First make sure you set 'SSL' to 'Full' and turn ON 'Automatic HTTPS Rewrites' in CloudFlare!${NC}"
echo ""
echo "${RED}*********************************************************************************************${NC}"
echo ""
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
echo "${GREEN}Then you should be all set up and ready to go!${NC}"
echo ""
echo "${GREEN}*********************************************************************************************${NC}"
echo ""
## Cleanup! ##
rm -r ../serversetupscript
exit